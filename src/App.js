import React,{useReducer} from 'react';
import './App.css';
import DataFetching from './DataFetching';
import FocusInput from './Components/FocusInput';
import ClassInput from './Components/ClassInput';
// import ContextplusReducer from './ContextplusReducer';
// import DataFetchingTwo from './DataFetchingTwo';
// import Counter3 from './Counter3';
// import Counter2 from './Components/Counter2';
// import Counter1 from './Counter1';
// import A  from './Components/ContextHoook';
// export const UserContext=React.createContext()
// export const ChannelContext=React.createContext()
export const CountContext=React.createContext()
const initialState=0
const reducer =(state,action)=>{
  switch(action){ 
  case 'increment':
    return state+1
  case 'decrement':
    return state-1
  default:
    return state
  
}

}

function App() {
  const [count, dispatch] = useReducer(reducer, initialState)
  return (
      
    // <CountContext.Provider value={{countState:count,countDispatch:dispatch}}>
    // <div className="App">
    //     <ContextplusReducer />
    //     <DataFetchingTwo />
    // </div>
    // </CountContext.Provider>
    <div>
    <DataFetching />
    <FocusInput />
    <ClassInput />
    </div>
  );
}

export default App;
