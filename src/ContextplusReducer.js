import React,{useContext} from 'react'
import {CountContext} from "./App"
function ContextplusReducer() {
    return (
        <div>
            <B/>
        </div>
    )
}


function B() {
    const countContext=useContext(CountContext)

    return (
        <div>
            <C />
            <h1>{countContext.countState}</h1>
            <button onClick={()=>countContext.countDispatch('increment')}>Increment</button>
            <button onClick={()=>countContext.countDispatch('decrement')}>Decrement</button>
        </div>
    )
}

function C() {
    const counters=useContext(CountContext)
    return (
        <div>
        <h1>  ContextplusReducer</h1>
        <h1>{counters.countState}</h1>
        <button onClick={()=>counters.countDispatch('increment')}>Increment</button>
        <button onClick={()=>counters.countDispatch('decrement')}>Decrement</button>
        
        </div>
    )
}


export default ContextplusReducer
