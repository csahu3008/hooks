import React,{useState,useEffect} from 'react'


function UseEffect() {
    const initialValue=0
    const [count,setCount]=useState(initialValue);
    useEffect(()=>{
        document.title=`You clicked ${count} times`
    });
    const incrementByfive=()=>{
          setCount((prevstate)=>prevstate+5)
    }
    return (
        <div>
          <p>You clicked {count} times</p>
          <button onClick={()=>setCount(count+1)}>Click mE</button>  
        <button onClick={incrementByfive}>Click Me</button>
        </div>
    )
}

export default UseEffect
