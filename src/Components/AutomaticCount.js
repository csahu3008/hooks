import React, { useState,useEffect } from 'react'

function AutomaticCount() {
    const [count,setCount]=useState(0)
    
    useEffect(() => {
      const  interval=setInterval(incrementCount,1000)   
       return () => {
            clearInterval(interval)
        };
    }, [])
    const incrementCount=()=>{
        setCount(prevCount => prevCount+1)
    }
    return (
        <div>
            <h1>{count}</h1>
        </div>
    )
}

export default AutomaticCount
