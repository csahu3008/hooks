import React, { useState } from 'react'

function HookArray() {
   const [items,setItems]=useState([])
   function addItem(){
       setItems([...items,{id:items.length,
    value:Math.floor(Math.random()*10)+1 
}])
   }
   return (
        <div>
        <button  onClick={addItem}>Add A number </button>
            {
                items.map(ele => (
                    <li key={ele.id}>{ele.value} </li>
                ))
            }
        </div>
    )
}

export default HookArray
