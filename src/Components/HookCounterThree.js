import React,{useState,useEffect} from 'react'

function HookCounterThree() {
    const [name,setName]=useState({firstName:'',lastname:""})
    const [na,setNa]=useState('')
    useEffect(() => {
        console.log("USeEffect Invoked")
    },[name.firstName,na])
    return (
        <div>
        <form>
        <input type='text' value={na} onChange={(e)=>{ setNa(e.target.value)} }/>
        <input type='text' value={name.firstName} onChange={(e)=>{setName({...name,firstName:e.target.value})}} />
        <input type='text' value={name.lastname} onChange={(e)=>{setName({...name,lastname:e.target.value})}} />

            <h1>Your name is {name.firstName}</h1>
            <h1>Your name is {name.lastname}</h1>
            <h1>{JSON.stringify(name)}</h1>
        </form>
        </div>
    )
}

export default HookCounterThree
