import React,{useState,useEffect} from 'react'
import axios from 'axios'
function DataFetching() {
    const [post,setPost]=useState({})
    const [id,setId]=useState(1)
    const [idfromButtonCLick,setIdFromButtonCLick]=useState(1)
    const handleChage=()=>{
        setIdFromButtonCLick(id)
   }
    useEffect(() => {
        axios.get(`https://jsonplaceholder.typicode.com/posts/${idfromButtonCLick}`).then(res =>{
            console.log(res)
            setPost(res.data)
        }).catch(err=>console.log(err))
    },[idfromButtonCLick])

    return (
        <div>
        <input type='text' value={id} onChange={e=>{setId(e.target.value)}} />
        <button type='button' onClick={handleChage} >Click ME</button>
           <p>{post.title}</p>
           
        </div>
    )
}

export default DataFetching
