import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class ClassInput extends Component {
    static propTypes = {

    }
    interval
    constructor(props) {
        super(props)
    
        this.state = {
             timer:0
        }
    }
    componentDidMount(){
        this.interval=setInterval(()=>{
            this.setState(prevsate=>({timer:prevsate.timer+1}))
        },1000)
    }
    componentWillMount(){
        clearInterval(this.interval)
    }

    render() {
        return (
            <div>
               class Timer -{this.state.timer}   
               <button onClick={()=>clearInterval(this.interval)} >Clear TImer </button>
            </div>
        )
    }
}

export default ClassInput
