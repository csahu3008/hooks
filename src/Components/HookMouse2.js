import React,{useState,useEffect} from 'react'
import HookMouse from './HookMouse'

function HookMouse2() {
    const [Display, setDisplay] = useState(true)
    // useEffect(()=>{

    // })
    return (
        <div>
            <button onClick={()=>setDisplay(!Display)}>Toggle Display</button>
            { Display && <HookMouse/> }
        </div>
    )
}

export default HookMouse2
