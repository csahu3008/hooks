import React, { Component } from 'react'

class ReactClass extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             count:0
        }
    }
    componentDidMount(){
        document.title=`you clicked me ${this.state.count}`;
    }
    componentWillUpdate(nextProps,nextStates){
        document.title=`you clicked me ${nextStates.count}`;
      
    }
    componentWillUnmount(){
        console.log(this.state.count)
    }
    // componentWillMount(){
    //     document.title=`you clicked me ${this.state.count}`
    // }
    render() {
        return (
            <div>
              <h3>You clicked me {this.state.count} times</h3>
                <button onClick={()=>{this.setState({count:this.state.count+1})}}>Click me</button>
            </div>
        )
    }
}

export default ReactClass
