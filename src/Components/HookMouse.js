import React, { useEffect, useState } from 'react'

function HookMouse() {
    const [x,setX]=useState(0)
    const [y,setY]=useState(0)
    const logMouse=(e)=>{
    console.log("mouse Event")
    setX(e.clientX);
    setY(e.clientY);
    }
//component will unmount
    useEffect(()=>{
        console.log("useEffect Called")
        window.addEventListener('mousemove',logMouse);
     return ()=>{
         console.log('component unounting code ');
         window.removeEventListener('mousemove',logMouse)
     }
    },[])
    return (
        <div>
            Hooks X -{x} Y -{y} 
        </div>
    )
}

export default HookMouse
