import React,{useContext} from 'react'
import {UserContext,ChannelContext} from '../App'
export default function A() {
    return (
        <div>
            <B />
        </div>
    )
}


export function B() {
    const user=useContext(UserContext)
    const channel=useContext(ChannelContext)
    return (
        <div>
            <C />
            <h1>{user}{channel}</h1> 
        </div>
    )
}
export function C() {
  
    return (
        <div>
        <UserContext.Consumer>
            {  
                user =>                    
                {
                    <ChannelContext.Consumer>
                        {
                            val =>{
                                return `Here is ${user}  coding in ${val}`
                            }
                        }
                    </ChannelContext.Consumer>
                }
            }
        </UserContext.Consumer>
        </div>
    )
}

